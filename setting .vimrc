On many Linux systems, like Ubuntu, the .vimrc file doesn't exist by default, so it is recommended that you create it first.

Don't use the .viminfo file that exist in the home directory. It is used for a different purpose.

Step 1: Go to your home directory

cd ~

Step 2: Create the file

vim .vimrc

Step 3: Add the configuration stated above

filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab
# for macOS and windows
set clipboard=unnamed
# for ubuntu
set clipboard=unnamedplus
Step 3: Save file, by pressing Shift + ZZ.


*** Configured .vimrc file with vundle. See vundle github page for more details. ***
# Begin
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'editorconfig/editorconfig-vim'
call vundle#end()

filetype plugin indent on
set tabstop=2
set shiftwidth=2
set noexpandtab
set clipboard=unnamed
set number
# End
